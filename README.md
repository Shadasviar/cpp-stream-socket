# cpp-stream-socket #

This library provides C++23 implementation of stream sockets that extends
`std::iostream`.

## Features ##
- Support of the iostream interface
- Support of IPV4, IPV6 and UNIX stream protocols

## How to use ##

### Building ###
This library is configured using CMake. You can add this library to your
repository as git submodule or install it system-widely command.

You can make this library static by setting `CPP_STREAM_SOCKET_LIB_TYPE` CMake
variable to `STATIC`. It is set to `SHARED` by default.

You can add this library by cmake `find_package(CppPosixSerial)` command.

### Usage ###
All you need is just create a `posix::net::socket` object with appropriate
parameters:

```c++
int main(int argc, char** argv) {
    posix::net::socket<side::SERVER, domain::IPV4> server;
    posix::net::socket<side::CLIENT, domain::IPV4> client;

    server.open({{"127.0.0.1", 5555}});
    client.open({{"127.0.0.1", 5555}});

    server << "abcd" << std::endl; /* Send string to the socket */

    std::string a;
    client >> a; /* a == "abcd"*/

    return 0;
}
```

Socket `open` method gets `config` structure that contains a domain-specific
address object and `backlog` that is set to `INT16_MAX` by default.

You can initialize socket from existing listening file descriptor by passing
it to `open` function instead of address:

```c++
  const std::string addr_str("/tmp/test_server_socket_from_fd");
  sockaddr_un addr = {0,0,};
  addr.sun_family = AF_UNIX;
  std::copy(addr_str.begin(), addr_str.end(), addr.sun_path);

  auto fd = ::socket(AF_UNIX, SOCK_STREAM, 0);
  ::bind(fd, addr, len);
  ::listen(fd, 10);

  posix::net::socket<side::SERVER, domain::UNIX> server;
  auto server_res = server.open(fd);
```

### Setting options ###
You can set socket option by method `set_option(int opt, T data)` where `opt`
is option name and `data` is argument that will be passed to `setsockopt` call.

```c++
 ... socket s ...
 auto ret = s.set_option(SO_LINGET, linger{1, 0});
 if (!ret) /*error*/
 ...
```

### Policies ###
Server socket supports different connection handling policies, that can be
switched by socket template parameter:

```c++
    posix::net::socket
        < side::server
        , domain::IPV4
        , type::STREAM
        , policy::single_thread
        > socket;
```

Available policies are:

- `multithread`: default server policy. It calls connection handler for new
connection in the new thread. This policy allows to have blocking handlers
that listen for data from connection until it closed. This policy allows to
handle many connections at the same time.
- `single_thread`: This policy calls connection handler directly from acceptor
thread. It is more lightweight than multithread, because it does not manage
multiple connections and does not create new trheads. Connection is closed
immediately after handler exits. If handler blocks, next connections will
not be handled.

### Getting peer info ###
Client sockets objects and all connection objects supports getting connected
peer information. Socket and connection object has `peer_info` method that
returns structure specific for connection address type. For `IPV4` and `IPV6`
sockets `peer_info_t` type is same as `addr_t` type: just peer address. For
`UNIX` sockets `peer_info_t` type corresponds to `unix_peer_info` structure
that contains peer's pid, program name and more other process information.

```c++
...

auto peer = client.peer_info();
peer->ip() == "127.0.0.1";
peer->port() == 1234;

...
```

#### Unix Domain Socket peer info ####
Unix domain sockets allows to get more peer information that other protocols.
Function `peer_info()` called on unix domain socket connections returns `unix_peer_info`
object that has methods like below:

- `std::string_view addr()`: returns string view with connection address.
- `std::string_view prog_name()`: get peer process name, obtained from `/proc/pid/cmdline` file
- `std::string_view username()`: get peer username at the connection moment.
- `std::string_view groupname()`: get peer group name at the connection moment.
- `pid_t pid()`: get peer's PID.
- `uid_t uid()`: get peer's UID.
- `gid_t gid()`: get peer's GID.

### Putting characters back ###
Putting charackters back to the socket is possible only by the `putback`
function. Ungetting the last character by `unget` function is not possible
and always puts stream into the bad state.

### This library does not support my favorite feature ###
If some feature is not directly supported by this library, you always can get
socket's file descriptor by `fd()` function and safely configure socket such
way you like.
