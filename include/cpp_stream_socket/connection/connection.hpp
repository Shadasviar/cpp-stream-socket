#ifndef STREAM_SOCKET_CONNECTION_HPP
#define STREAM_SOCKET_CONNECTION_HPP

#include <iostream>
#include <functional>
#include <vector>
#include <atomic>

#include <cpp_stream_socket/exit_code.hpp>
#include <cpp_stream_socket/utils/notifier.hpp>

namespace posix::net {

  class connection : public std::iostream, private std::streambuf {
    public:

      using handler = std::function<void(result<connection>&&)>;

      connection();
      connection& operator=(connection &&rhs);
      connection(connection &&rhs);
      ~connection();

      static result<connection> create(int fd) noexcept;

      void close() noexcept;
      int fd() const noexcept;
      bool is_open() const noexcept;

      template <typename addr_t>
      result<typename addr_t::peer_info_t> peer_info() const {
        return addr_t::peer_info(_fd);
      }

      int sync() override;
      int overflow(int c) override;
      std::streamsize xsputn(const char* s, std::streamsize n) override;
      int underflow() override;
      int uflow() override;
      std::streamsize xsgetn(char* s, std::streamsize n) override;
      int pbackfail(int c) override;
      std::streamsize showmanyc() override;

    private:

      int read_all_available_data();

      static result<connection> init(int fd, notifier &&exit_event);

      int               _fd         = -1;
      std::atomic<bool> _is_open    = false;
      std::vector<char> _in_buf;
      std::vector<char> _out_buf;
      notifier          _exit_event;
  };

} /* namespace posix::net */

#endif /* STREAM_SOCKET_CONNECTION_HPP */
