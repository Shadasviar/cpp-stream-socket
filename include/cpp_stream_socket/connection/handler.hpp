#ifndef CPP_STREAM_SOCKET_CONNECTION_HANDLER_HPP
#define CPP_STREAM_SOCKET_CONNECTION_HANDLER_HPP

#include <functional>

#include <cpp_stream_socket/connection/connection.hpp>
#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net::detail {

  /**
   * @brief Internal class that implements functions for connection handling.
   *
   * @note Do not use it in user code, do not use it in polimorphic context.
   */
  class connection_handler {
    public:

      /**
       * @brief Set connection handler function. This function will be called
       * for each accepted connection after set_handler call from a
       * new thread.
       * This function should be call before handler is started to avoid
       * data races for handler.
       */
      void set_handler(const connection::handler &h);

      /**
       * @brief Invoke handler function stored in this object.
       */
      void invoke(result<connection> &&conn);

    protected:
      connection::handler _handler;
  };

} /* namespace posix::net */

#endif /* CPP_STREAM_SOCKET_CONNECTION_HANDLER_HPP */
