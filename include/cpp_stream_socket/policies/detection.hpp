#ifndef STREAM_SOCKET_CONNCETOR_DETECTION_HPP
#define STREAM_SOCKET_CONNCETOR_DETECTION_HPP

#include <cpp_stream_socket/config.hpp>
#include <cpp_stream_socket/policies/empty.hpp>
#include <cpp_stream_socket/policies/multithread.hpp>
#include <cpp_stream_socket/policies/single_thread.hpp>
#include <cpp_stream_socket/policies/client.hpp>

namespace posix::net {

  template <side S, type T>
  struct detect_policy {
    using type = policy::empty;
  };

  template <type T>
  struct detect_policy<side::CLIENT, T> {
    using type = policy::client;
  };

  template <>
  struct detect_policy<side::SERVER, type::STREAM> {
    using type = policy::multithread;
  };

} /* namespace posix::net */

#endif /* STREAM_SOCKET_CONNCETOR_DETECTION_HPP */
