#ifndef STREAM_SOCKET_POLICY_MULTITHREAD_HPP
#define STREAM_SOCKET_POLICY_MULTITHREAD_HPP

#include <list>
#include <unordered_set>
#include <thread>
#include <mutex>
#include <atomic>
#include <streambuf>
#include <type_traits>

#include <cpp_stream_socket/exit_code.hpp>
#include <cpp_stream_socket/utils/notifier.hpp>
#include <cpp_stream_socket/connection/connection.hpp>
#include <cpp_stream_socket/connection/handler.hpp>

namespace posix::net::policy {

  /**
   * @brief Handle connections by creating a new thread for each conncetion.
   * Connection handlers can perform blocking reading and the times of other
   * connections would not be affected.
   *
   * This policy is useful for high-responsivness communication where each
   * connection should be handled in near real time. Each connection exists
   * in a separate thread, so the connection handler will not wait for
   * end of previous connection processing.
   */
  class multithread
    : public detail::connection_handler
    , public std::streambuf
  {
    public:

      using handler = connection::handler;

      /**
       * @brief Start acceptor listening for connections in a new thread.
       * When acceptor is running, each new connection will be handled by a
       * new thread and passed to handler function that is available at the
       * moment of connection creating.
       *
       * @param fd Listenig socket file descriptor after.
       *
       * @return Returns nothing when acceptor successfully started, or error
       * code in case of error.
       */
      [[nodiscard]] result<void> enable(int fd);

      multithread() = default;
      ~multithread();

      /**
       * @brief Close connections acceptor and all running connections. This
       * function will wait for all handlers end, so user code must sure that
       * handler does not block forever.
       */
      void close() noexcept;

      int fd() const noexcept;
      size_t n_connections() const noexcept;

      int sync() override;
      int overflow(int c) override;
      std::streamsize xsputn(const char* s, std::streamsize n) override;
      int underflow() override;
      int uflow() override;
      std::streamsize xsgetn(char* s, std::streamsize n) override;
      int pbackfail(int c) override;
      std::streamsize showmanyc() override;

    private:

      void acceptor(int fd);
      void collect_garbage();
      result<void> start_acceptor(int fd) noexcept;
      result<void> start_notifier(notifier &&x) noexcept;

      /**
       * @brief This helper structure pairs connection with its
       * handler's thread.
       */
      struct running_connection {

        template <typename ... Ts>
        running_connection(Ts&& ... args)
          : conn(connection::create(std::forward<Ts...>(args...)))
          {
        }

        ~running_connection() {
          if (conn) conn->close();
          if (thread.joinable()) thread.join();
        }

        result<connection>  conn;
        std::thread         thread;
      };

      template <typename F>
      void fmap_connections(F &&f) {
        for (auto &c : _connections) {
          if (!c.conn) continue;
          std::invoke(std::forward<F>(f), *c.conn);
        }
      }

      template <typename F, typename T = std::invoke_result_t<F, connection&>>
      T fmap_first_connection(F &&f) {
        for (auto &c : _connections) {
          if (!c.conn) continue;
          return std::invoke(std::forward<F>(f), *c.conn);
        }
        return EOF;
      }

      std::mutex                    _connections_lock;
      std::list<running_connection> _connections;
      std::thread                   _acceptor;
      std::thread                   _garbage_collector;
      std::atomic<bool>             _stop = true;
      notifier                      _notifier;
      notifier                      _acceptor_ready;
      int                           _fd = -1;
  };

} /* posix::net::policy */

#endif /* STREAM_SOCKET_POLICY_MULTITHREAD_HPP */
