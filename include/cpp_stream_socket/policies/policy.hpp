#ifndef CPP_STREAM_SOCKET_POLICY_HPP
#define CPP_STREAM_SOCKET_POLICY_HPP

#include <type_traits>
#include <streambuf>
#include <concepts>

#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net::policy {

  template <typename T>
  concept policy
     = std::is_base_of_v<std::streambuf, T>
    && requires (T a, int x) {
      {a.enable(x)} -> std::same_as<result<void>>;
      {a.close()};
      {a.fd()} -> std::same_as<int>;
      {a.n_connections()} -> std::same_as<size_t>;
    };

} /* namespace posix::net::policy */

#endif /* CPP_STREAM_SOCKET_POLICY_HPP */
