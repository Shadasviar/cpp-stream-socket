#ifndef CPP_STREAM_SOCKET_CLIENT_POLICY_HPP
#define CPP_STREAM_SOCKET_CLIENT_POLICY_HPP

#include <streambuf>

#include <cpp_stream_socket/connection/connection.hpp>

namespace posix::net::policy {

  /**
   * @brief This policy represents a client connection that is used with
   * client sockets.
   */
  class client : public std::streambuf {

    public:

      client() = default;
      ~client();

      /**
       * @brief Create connection object for alerady conneted file descriptor.
       *
       * @param fd Already connected client socket file descriptor.
       *
       * @return Returns nothing on success, or error code in case of error.
       */
      [[nodiscard]] result<void> enable(int fd) noexcept;

      /**
       * @brief Close client connection.
       */
      void close() noexcept;

      int fd() const noexcept;

      size_t n_connections() const noexcept;

      /**
       * @brief Get connection reference that can be used for communication.
       */
      [[nodiscard]] connection& conn() noexcept;

      int sync() override;
      int overflow(int c) override;
      std::streamsize xsputn(const char* s, std::streamsize n) override;
      int underflow() override;
      int uflow() override;
      std::streamsize xsgetn(char* s, std::streamsize n) override;
      int pbackfail(int c) override;
      std::streamsize showmanyc() override;

    private:

      result<void> set_connection(connection &&conn);

      connection _connection;
  };

} /* namespace posix::net::policy */

#endif /* CPP_STREAM_SOCKET_CLIENT_POLICY_HPP */
