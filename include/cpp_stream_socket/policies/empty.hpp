#ifndef STREAM_SOCKET_POLICY_EMPTY_HPP
#define STREAM_SOCKET_POLICY_EMPTY_HPP

namespace posix::net::policy {

  /**
   * @brief Empty policy is used for client sockets or for connectionless
   * socket types. It just do nothing and unwinds to nothing
   * during compilation.
   */
  struct empty {
  };

} /* namespace posix::net::policy */

#endif /* STREAM_SOCKET_POLICY_EMPTY_HPP */
