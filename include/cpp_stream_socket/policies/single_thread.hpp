#ifndef STREAM_SOCKET_POLICY_SINGLE_THREAD_HPP
#define STREAM_SOCKET_POLICY_SINGLE_THREAD_HPP

#include <thread>
#include <streambuf>
#include <type_traits>
#include <atomic>

#include <cpp_stream_socket/exit_code.hpp>
#include <cpp_stream_socket/utils/notifier.hpp>
#include <cpp_stream_socket/connection/connection.hpp>
#include <cpp_stream_socket/connection/handler.hpp>

namespace posix::net::policy {

  /**
   * @brief Handle connections by running handler in acceptor thread.
   * Conncetion handler should exit as fast as possible for not blocking
   * other connections from beign handled.
   *
   * This policy is useful when communication is based on fast processing
   * connection and closing it immediately. This policy does not create
   * unnecessary thread in such case.
   */
  class single_thread
    : public detail::connection_handler
    , public std::streambuf
  {
    public:

      using handler = connection::handler;

      /**
       * @brief Start acceptor listening for connections in a new thread.
       * When acceptor is running, each new connection will be handled by the
       * same thread.
       *
       * @param fd Listenig socket file descriptor after.
       *
       * @return Returns nothing when acceptor successfully started, or error
       * code in case of error.
       */
      [[nodiscard]] result<void> enable(int fd) noexcept;

      ~single_thread();

      /**
       * @brief Close connections acceptor and current running connection. This
       * function will wait for current handler end, so user code must sure that
       * handler does not block forever.
       */
      void close() noexcept;

      int fd() const noexcept;
      size_t n_connections() const noexcept;

      int sync() override;
      int overflow(int c) override;
      std::streamsize xsputn(const char* s, std::streamsize n) override;
      int underflow() override;
      int uflow() override;
      std::streamsize xsgetn(char* s, std::streamsize n) override;
      int pbackfail(int c) override;
      std::streamsize showmanyc() override;

    private:

      void acceptor(int fd) noexcept;
      result<void> start_acceptor(int fd) noexcept;
      result<void> start_notifier(notifier &&x) noexcept;

      std::thread               _acceptor;
      std::atomic<bool>         _stop = true;
      notifier                  _notifier;
      notifier                  _acceptor_ready;
      int                       _fd = -1;
  };

} /* posix::net::policy */

#endif /* STREAM_SOCKET_POLICY_SINGLE_THREAD_HPP */
