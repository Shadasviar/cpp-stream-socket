#ifndef CPP_STREAM_SOCKET_OS_POSIX_HPP
#define CPP_STREAM_SOCKET_OS_POSIX_HPP

#include <sys/socket.h>

#include <cpp_stream_socket/config.hpp>
#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net {

  [[nodiscard]] result<int> create_socket(domain d, type t) noexcept;
  [[nodiscard]] result<int> bind(const address &addr, int fd) noexcept;
  [[nodiscard]] result<int> connect(const address &addr, int fd) noexcept;

  template <detail::socket_address addr_t, type t>
  [[nodiscard]] result<int> listen(const config<addr_t> &cfg, int fd) {
    if constexpr (t == type::STREAM) {
      auto ret = ::listen(fd, cfg.backlog);
      if (ret < 0) return make_error(error_code::LISTEN_FAILED);
    }
    return fd;
  }

} /* namespace posix::net */

#endif /* CPP_STREAM_SOCKET_OS_POSIX_HPP */
