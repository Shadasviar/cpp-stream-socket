#ifndef CPP_STREAM_SOCKET_UTILS_SOCKETPAIR_HPP
#define CPP_STREAM_SOCKET_UTILS_SOCKETPAIR_HPP

#include <cstdint>
#include <array>
#include <atomic>

#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net {

  class notifier {
    public:

      enum class type : uint8_t {
        NOTIFIER,
        RECEIVER,
      };


      notifier() = default;
      notifier& operator=(notifier &&rhs);
      notifier(notifier &&rhs);

      ~notifier();

      /**
       * @brief Create notifier instance with error handling.
       */
      static result<notifier> create() noexcept;
      
      /**
       * @brief Get file descriptor of given side of notifier.
       */
      int get_fd(type t) noexcept;

      /**
       * @brief Notify receiver by seending it 1 byte.
       */
      result<void> notify() noexcept;

      void close() noexcept;

    private:
      std::array<int, 2> _fds;
      std::atomic<bool>  _state_is_valid = false;
  };

} /* namespace posix::net */

#endif /* CPP_STREAM_SOCKET_UTILS_SOCKETPAIR_HPP */
