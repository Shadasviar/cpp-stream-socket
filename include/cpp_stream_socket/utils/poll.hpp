#ifndef CPP_STREAM_SOCKET_UTILS_POLL_HPP
#define CPP_STREAM_SOCKET_UTILS_POLL_HPP

#include <unordered_set>
#include <unordered_map>
#include <vector>

#include <poll.h>

#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net::detail {

  using event_t = decltype(::pollfd::events);

  class pollfds {
    public:

      using triggered_fds = std::unordered_set<int>;
      using events_for_fd = std::unordered_map<int, std::unordered_set<event_t>>;

      pollfds(int timeout, const events_for_fd &expected_events);

      result<triggered_fds> operator()();

    private:
      using folded_events = std::unordered_map<int, event_t>;

      int                   _timeout;
      folded_events         _expected_events;
      std::vector<::pollfd> _pfds;
  };

} /* namespace detail */

#endif /* CPP_STREAM_SOCKET_UTILS_POLL_HPP */
