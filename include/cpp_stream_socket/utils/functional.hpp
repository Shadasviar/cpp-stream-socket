#ifndef STREAM_SOCKET_UTILS_FUNCTIONAL_HPP
#define STREAM_SOCKET_UTILS_FUNCTIONAL_HPP

#include <functional>

#define bind_this(f, ...) \
  detail::bind_obj(f, std::ref(*this) __VA_OPT__(,) __VA_ARGS__)

namespace posix::net::detail {

    template <typename F, typename ... Ts>
    auto bind_obj(F&& f, Ts&& ... xs) noexcept {
      return std::bind_front
        ( std::mem_fn(std::forward<F>(f))
        , std::forward<Ts>(xs)...
        );
    }

    template <typename F, typename T>
    auto mbind(F &&f, T &&x) {
      using ret_t = decltype(std::invoke(std::forward<F>(f), *x));
      if (!x) return ret_t(std::unexpect, x.error());
      return std::invoke(std::forward<F>(f), *x);
    }

    template
      < typename F
      , typename T
      , typename ... Ts
      , typename ret_t = std::invoke_result_t
        < F
        , typename T::value_type
        , typename Ts::value_type ...
        >
      >
    auto mbind(F &&f, T &&x, Ts&& ... xs) {
      if (!x) return ret_t(std::unexpect, x.error());
      return mbind
        ( std::bind_front(std::forward<F>(f), *x)
        , std::forward<Ts>(xs) ...
        );
    }

} /* namespace detail */

#endif /* STREAM_SOCKET_UTILS_FUNCTIONAL_HPP */
