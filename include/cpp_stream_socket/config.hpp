#ifndef STREAM_SOCKET_CONFIG_HPP
#define STREAM_SOCKET_CONFIG_HPP

#include <functional>
#include <filesystem>
#include <string_view>
#include <utility>

#include <sys/socket.h>

#include <cpp_stream_socket/address/address.hpp>

namespace posix::net {

  /**
   * @brief Socket side in communication: server or client.
   */
  enum class side {
    CLIENT,
    SERVER,
  };

  /**
   * @brief Socket domain that wraps POSIX domains.
   */
  enum class domain : int {
    UNIX  = AF_UNIX,  /**< Unix Domain Socket.*/
    IPV4  = AF_INET,  /**< IPv4 Internet protocol.*/
    IPV6  = AF_INET6, /**< IPv6 Internet protocol.*/
  };

  inline bool operator==(domain lhs, int rhs) {
    return std::to_underlying(lhs) == rhs;
  }

  inline bool operator==(int lhs, domain rhs) {
    return rhs == lhs;
  }

  /**
   * @brief Socket type that wraps POSIX socket types.
   *
   * @note Regardless ths library implements `iostream` interface and has name
   * `stream` socket, it allows to use non-stream socket types. Of course, you
   * must to remember the limitations of another sockets types such an
   * unspecified messages order in UDP protocol and do not use it in
   * stream-like way. Do not use stream operators for UDP protocols, use
   * `read`, `write` etc. iostream functions.
   */
  enum class type : int {
    STREAM    = SOCK_STREAM,  /**< Reliable, sequenced, connected streams.*/
    DATAGRAM  = SOCK_DGRAM,   /**< Unreliable, connectionnless messages*/
  };

  /**
   * @brief Possible states of socket. It is mainly used in socket's internals.
   */
  enum class state {
    INVALID,  /**< Socket is uninitialized, closed or moved out. Do not use it.*/
    ACTIVE,   /**< Socket is initialized and active.*/
  };

  template <detail::socket_address A>
  struct config {
    A   addr;                 /**< Socket address dependent on domain.*/
    int backlog = INT16_MAX;  /**< Maximum number of connection for server.*/
  };

  namespace detail {

    static constexpr int poll_timeout = 1e3;

  } /* namespace detail */

} /* namespace net */

#endif /* STREAM_SOCKET_CONFIG_HPP */
