#ifndef STREAM_SOCKET_IMPL_HPP
#define STREAM_SOCKET_IMPL_HPP

#include <iostream>
#include <string_view>
#include <filesystem>
#include <functional>
#include <utility>
#include <atomic>

#include <cpp_stream_socket/config.hpp>
#include <cpp_stream_socket/address/address.hpp>
#include <cpp_stream_socket/address/detection.hpp>
#include <cpp_stream_socket/exit_code.hpp>
#include <cpp_stream_socket/policies/detection.hpp>
#include <cpp_stream_socket/policies/policy.hpp>
#include <cpp_stream_socket/utils/functional.hpp>
#include <cpp_stream_socket/os/posix.hpp>

namespace posix::net {

  template
    < side                    side_v
    , domain                  domain_v
    , type                    type_v    = type::STREAM
    , policy::policy          policy_t  = detect_policy<side_v, type_v>::type
    , detail::socket_address  addr_t    = detect_address<domain_v>::type
    >
  class socket : public std::iostream, private std::streambuf {
    public:

      using ref = std::reference_wrapper<socket>;

      explicit socket() : std::iostream(this) {
      }

      ~socket() {
        close();
      }

      state status() const {
        return _state;
      }

      /**
       * @brief Returns number of active connections of the socket. In case
       * of client socket it is 1 is is is connected or 0 otherwise.
       */
      size_t n_connections() const noexcept {
        return _policy.n_connections();
      }

      template <typename T = void> requires (side_v == side::SERVER)
      /**
       * @brief Open socket and start listening on events if event handlers
       * are connected.
       *
       * @param cfg Socket configuration structure. It depends on socket's
       * domain.
       */
      [[nodiscard]] result<int> open(const config<addr_t> &cfg) noexcept {
        try {
          if (_state == state::ACTIVE) return _policy.fd();
          _addr = cfg.addr;

          return detail::mbind
            ( bind
            , cfg.addr.posix_addr()
            , create_socket(domain_v, type_v)
            )
            .and_then(std::bind_front(listen<addr_t, type_v>, cfg))
            .and_then(bind_this(&socket::enable))
            ;
        } catch (const std::exception &e) {
          return make_error(error_code::UNKNOWN_ERROR, e);
        }
      }

      template <typename T = void> requires (side_v == side::SERVER)
      /**
       * @brief Create socket from existing listening file descriptor.
       *
       * @param fd Existing socket file descriptor that must be opened by
       * `socket` call, binded to address and in listening state.
       */
      [[nodiscard]] result<int> open(int fd) noexcept {
        try {
          if (_state == state::ACTIVE) return _policy.fd();

          return check_type(fd)
            .and_then(bind_this(&socket::get_addr))
            .and_then(bind_this(&socket::enable))
            ;
        } catch (const std::exception &e) {
          return make_error(error_code::UNKNOWN_ERROR, e);
        }
      }

      template <typename T = void> requires (side_v == side::CLIENT)
      /**
       * @brief Open socket and connect to a server for connection-based
       * sockets, or remember address for connectionless sockets.
       *
       * @param cfg Socket configuration structure. It depends on socket's
       * domain.
       */
      [[nodiscard]] result<int> open(const config<addr_t> &cfg) noexcept {
        try {
          if (_state == state::ACTIVE) return _policy.fd();
          _addr = cfg.addr;

          return detail::mbind
            ( connect
            , cfg.addr.posix_addr()
            , create_socket(domain_v, type_v)
            )
            .and_then(bind_this(&socket::enable))
            ;
        } catch (const std::exception &e) {
          return make_error(error_code::CONNECTION_FAILED, e);
        }
      }

      template <typename T>
      [[nodiscard]] result<void> set_option(int opt, T &&data) noexcept {
        if (_state == state::INVALID)
          return make_error(error_code::SETSOCKOPT_FAILED);

        auto ret = ::setsockopt(fd(), SOL_SOCKET, opt, &data, sizeof(data));
        if (ret < 0) return make_error(error_code::SETSOCKOPT_FAILED);

        return {};
      }

      [[nodiscard]] int fd() const noexcept {
        return _policy.fd();
      }

      template <typename T = void> requires (side_v != side::SERVER)
      [[nodiscard]] result<typename addr_t::peer_info_t> peer_info() {
        return addr_t::peer_info(_policy.fd());
      }

      [[nodiscard]] addr_t address() const {
        return _addr;
      }

      template <typename T = void> requires (side_v == side::SERVER)
      /**
       * @brief Set connection handler that will be called when server
       * accepts a new connection.
       *
       * @param f Function that should handle connection.
       */
      void on_connected(const connection::handler &f) {
        _policy.set_handler(f);
      }

      void close() noexcept {
        if (_state == state::ACTIVE) {
          _state = state::INVALID;
          _policy.close();

          if constexpr (side_v == side::SERVER) {
            _addr.close();
          }
        }
      }

    private:

      [[nodiscard]] result<int> check_type(int fd) {
        int val {0};
        socklen_t len = sizeof(val);

        auto ret = ::getsockopt(fd, SOL_SOCKET, SO_ACCEPTCONN, &val, &len);
        if (ret < 0) return make_error(error_code::GETSOCKOPT_FAILED);
        if (!val)
          return make_error
            ( error_code::FD_IS_NOT_LISTENING
            , std::runtime_error("fd is not listening")
            );

        ret = ::getsockopt(fd, SOL_SOCKET, SO_TYPE, &val, &len);
        if (ret < 0) return make_error(error_code::GETSOCKOPT_FAILED);
        if (val != std::to_underlying(type_v))
          return make_error
            ( error_code::TYPE_MISMATCH
            , std::runtime_error
              ( "fd type mismatch: got "
              + std::to_string(val)
              + ", expected "
              + std::to_string(std::to_underlying(type_v)))
            );

        return fd;
      }

      [[nodiscard]] result<int> get_addr(int fd) {
        auto addr = address_from_fd<addr_t>(fd);
        if (!addr) return std::unexpected(addr.error());
        _addr = *addr;
        return fd;
      }

      [[nodiscard]] result<int> enable(int fd) {
        auto enable_ret = _policy.enable(fd);
        if (!enable_ret) return std::unexpected(enable_ret.error());

        _state = state::ACTIVE;
        return fd;
      }

      int sync() override {
        return _policy.sync();
      }

      int overflow(int c) override {
        return _policy.overflow(c);
      }

      std::streamsize xsputn(const char* s, std::streamsize n) override {
        return _policy.xsputn(s, n);
      }

      int underflow() override {
        return _policy.underflow();
      }

      int uflow() override {
        return _policy.uflow();
      }

      std::streamsize xsgetn(char* s, std::streamsize n) override {
        return _policy.xsgetn(s, n);
      }

      int pbackfail(int c) override {
        return _policy.pbackfail(c);
      }

      std::streamsize showmanyc() override {
        return _policy.showmanyc();
      }

      std::atomic<state>  _state      = state::INVALID;
      policy_t            _policy;
      addr_t              _addr;
  };

} /* namespace posix::net */

#endif /* STREAM_SOCKET_IMPL_HPP */
