#ifndef STREAM_SOCKET_EXIT_CODE_HPP
#define STREAM_SOCKET_EXIT_CODE_HPP

#include <expected>
#include <unordered_map>
#include <string>
#include <cstring>
#include <variant>

#include <errno.h>

namespace posix::net {

  /**
   * @brief Error codes reported by the library.
   */
  enum class error_code {
    CREATE_SOCKET_FAILED,
    INVALID_ADDRESS,
    BIND_FAILED,
    LISTEN_FAILED,
    ACCEPT_FAILED,
    CONNECTION_FAILED,
    MT_POLICY_FAILED,
    ST_POLICY_FAILED,
    POLL_FAILED,
    NOTIFIER_FAILED,
    SETSOCKOPT_FAILED,
    GETSOCKOPT_FAILED,
    FD_IS_NOT_LISTENING,
    TYPE_MISMATCH,
    GETSOCKNAME_FAILED,
    GET_PEER_INFO_FAILED,
    GET_PEER_CREDS_FAILED,
    UNKNOWN_ERROR,
  };

  class error {
    public:

      explicit error(error_code code) noexcept;
      explicit error(error_code code, const std::exception &e) noexcept;

      /**
       * @brief Return textual reason of the error. Error reason is a textual
       * representation of errno related to given error code.
       */
      std::string reason() const;

      /**
       * @brief Return textual error message stored in the error object.
       */
      std::string msg() const;

    private:
      using reason_t = std::variant<int, std::exception>;

      struct reason_visitor {
        std::string operator()(int errnum) const;
        std::string operator()(const std::exception &e) const;
      };

      error_code  _code;
      reason_t    _reason;
  };

  template <typename T>
  using result = std::expected<T, error>;

  /**
   * @brief Helper function for creating std::unexpected values from error
   * objects.
   */
  inline auto make_error(error_code x, auto ... args) noexcept {
    return std::unexpected {error {x, args ...}};
  }

} /* namespace posix::net */

#endif /* STREAM_SOCKET_EXIT_CODE_HPP */
