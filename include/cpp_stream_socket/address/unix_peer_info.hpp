#ifndef STREAM_SOCKET_UNIX_PEER_INFO_HPP
#define STREAM_SOCKET_UNIX_PEER_INFO_HPP

#include <string_view>

#include <sys/socket.h>

#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net {

  class unix_peer_info {
    public:

      unix_peer_info
        ( std::string_view path
        , const ucred &creds
        , std::string_view progname
        , std::string_view username
        , std::string_view groupname
        );

      /**
       * @brief Get peer socket address.
       */
      std::string_view addr() const;

      /**
       * @brief Get peer socket program name extracted from /proc/pid/cmdline
       */
      std::string_view progname() const;

      /**
       * @brief Get effective peer's user name.
       */
      std::string_view username() const;

      /**
       * @brief Get effective peer's group name.
       */
      std::string_view groupname() const;

      pid_t pid() const;
      uid_t uid() const;
      gid_t gid() const;

    private:

      std::string _str_addr;
      ucred       _creds;
      std::string _progname;
      std::string _username;
      std::string _groupname;
  };

  /**
   * @brief Create peer info object for unix domain socket.
   *
   * @param fd Connection file descriptor for getting peer data.
   * @return If peer data was extracted successfully, returns `unix_peer_info`
   * object. In case of error, returns error object.
   */
  result<unix_peer_info> make_unix_peer_info(int fd);

  bool operator==(const unix_peer_info &lhs, const unix_peer_info &rhs);

} /* namespace posix::net */

#endif /* STREAM_SOCKET_UNIX_PEER_INFO_HPP */
