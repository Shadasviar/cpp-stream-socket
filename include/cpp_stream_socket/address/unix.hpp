#ifndef STREAM_SOCKET_UNIX_ADDRESS_HPP
#define STREAM_SOCKET_UNIX_ADDRESS_HPP

#include <string>
#include <string_view>

#include <cpp_stream_socket/address/address.hpp>
#include <cpp_stream_socket/address/unix_peer_info.hpp>

namespace posix::net {

  class unix_address {
    public:
      using peer_info_t = unix_peer_info;

      unix_address() = default;
      unix_address(std::string_view path);
      unix_address(const sockaddr* addr, socklen_t len);

      static result<peer_info_t> peer_info(int fd);
      result<address> posix_addr() const;
      void close();

      std::string_view addr() const;

      auto operator<=>(const unix_address&) const = default;

    private:

      std::string _str_addr;
  };

} /* namespace posix::net */

#endif /* STREAM_SOCKET_UNIX_ADDRESS_HPP */
