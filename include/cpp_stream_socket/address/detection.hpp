#ifndef STREAM_SOCKET_ADDRESS_DETECTION_HPP
#define STREAM_SOCKET_ADDRESS_DETECTION_HPP

#include <cpp_stream_socket/config.hpp>
#include <cpp_stream_socket/exit_code.hpp>
#include <cpp_stream_socket/address/address.hpp>
#include <cpp_stream_socket/address/ipv4.hpp>
#include <cpp_stream_socket/address/ipv6.hpp>
#include <cpp_stream_socket/address/unix.hpp>

namespace posix::net {

  template <domain T>
  struct detect_address {};

  template <>
  struct detect_address<domain::IPV4> {
    using type = ipv4_address;
  };

  template <>
  struct detect_address<domain::IPV6> {
    using type = ipv6_address;
  };

  template <>
  struct detect_address<domain::UNIX> {
    using type = unix_address;
  };

  template <detail::socket_address addr_t>
  /**
   * @brief Dynamically detect address family and make an address object from
   * existing fil descriptor.
   */
  [[nodiscard]] result<addr_t> address_from_fd(int fd) {
    sockaddr_storage addr_storage;
    socklen_t len = sizeof(addr_storage);
    auto addr = reinterpret_cast<sockaddr*>(&addr_storage);

    auto ret = ::getsockname(fd, addr, &len);
    if (ret < 0) return make_error(error_code::GETSOCKNAME_FAILED);

    try {
      return addr_t(addr, len);
    } catch (const std::exception& e) {
      return make_error(error_code::INVALID_ADDRESS, e);
    }
  }

} /* namespace posix::net */

#endif /* STREAM_SOCKET_ADDRESS_DETECTION_HPP */
