#ifndef STREAM_SOCKET_ADDRESS_HPP
#define STREAM_SOCKET_ADDRESS_HPP

#include <concepts>

#include <sys/socket.h>
#include <cstring>

#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net {

  struct address {

    const sockaddr* ptr() const {
      return reinterpret_cast<const sockaddr*>(&raw_addr);
    }

    sockaddr_storage  raw_addr;
    socklen_t         length;
  };

  namespace detail {

    template <typename T>
    concept socket_address = requires (T t, const sockaddr* addr, socklen_t len, int fd) {
      {t.posix_addr()}  -> std::same_as<result<address>>;
      {T(addr, len)}    -> std::same_as<T>;
      t.close();
      {T::peer_info(fd)} -> std::same_as<result<typename T::peer_info_t>>;
    };

  } /* namespace detail */

} /* namespace posix::net */

#endif /* STREAM_SOCKET_ADDRESS_HPP */
