#ifndef STREAM_SOCKET_IPV6_ADDRESS_HPP
#define STREAM_SOCKET_IPV6_ADDRESS_HPP

#include <string>
#include <string_view>

#include <cpp_stream_socket/address/address.hpp>

#include <arpa/inet.h>

namespace posix::net {

  class ipv6_address {
    public:
      using peer_info_t = ipv6_address;

      ipv6_address() = default;
      ipv6_address(std::string_view ip, int port);
      ipv6_address(const sockaddr* addr, socklen_t len);

      static result<peer_info_t> peer_info(int fd);
      result<address> posix_addr() const;
      void close();

      std::string_view addr() const;
      in_port_t port() const;

      auto operator<=>(const ipv6_address&) const = default;

    private:

      std::string _str_addr;
      in_port_t   _port = -1;
  };

} /* namespace posix::net */

#endif /* STREAM_SOCKET_IPV6_ADDRESS_HPP */

