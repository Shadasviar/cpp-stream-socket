#ifndef STREAM_SOCKET_TESTS_GTEST_HELPERS_HPP
#define STREAM_SOCKET_TESTS_GTEST_HELPERS_HPP

#include <gtest/gtest.h>

#include <iostream>
#include <thread>
#include <chrono>

#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net::tests::detail {

  template <typename T>
  void check_res(const result<T> &res) {
    ASSERT_TRUE(res.has_value())
      << res.error().msg() << " (" << res.error().reason() << ")\n";
  }

} /* namespace posix::net::tests::detail */

#define ASSERT_RESULT_OK(res) posix::net::tests::detail::check_res(res)

#define START_SOCKETS(x) \
  x->open_server(); \
  ASSERT_EQ(x->server.status(), state::ACTIVE); \
  ASSERT_TRUE(x->server.good()); \
  x->open_client(); \
  ASSERT_EQ(x->client.status(), state::ACTIVE); \
  ASSERT_TRUE(x->client.good()); \

inline void wait_for_data(std::istream &s, uint16_t n_retry = 1000) {
  for (uint16_t i = 0; i < n_retry && s.peek() == EOF; ++i) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    s.clear();
  }
}

#endif /* STREAM_SOCKET_TESTS_GTEST_HELPERS_HPP */
