#ifndef STREAM_SOCKET_TEST_SUITE_H
#define STREAM_SOCKET_TEST_SUITE_H

#include <gtest/gtest.h>

#include <gtest_helpers.hpp>

#include <unistd.h>
#include <fstream>

#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

#include <cpp_stream_socket/socket.hpp>

using namespace posix::net;

template <domain d>
struct test_params {
  static constexpr domain domain_v = d;
  using addr = detect_address<d>::type;
};

template <typename T>
class multithread_test_suite : public testing::Test {
  public:

    posix::net::socket<side::SERVER, T::domain_v> server;
    posix::net::socket<side::CLIENT, T::domain_v> client;

    template <typename G = void> requires (T::domain_v == domain::IPV4)
    static ipv4_address test_addr() {
      return ipv4_address("127.0.0.1", 5555);
    }

    template <typename G = void> requires (T::domain_v == domain::IPV6)
    static ipv6_address test_addr() {
      return ipv6_address("::1", 5556);
    }

    template <typename G = void> requires (T::domain_v == domain::UNIX)
    static unix_address test_addr() {
      return unix_address("/tmp/cpp_stream_socket_test.socket");
    }

    template <typename G = void> requires (T::domain_v == domain::IPV4)
    static ipv4_address::peer_info_t expected_peer() {
      return test_addr();
    }

    template <typename G = void> requires (T::domain_v == domain::IPV6)
    static ipv6_address::peer_info_t expected_peer() {
      return test_addr();
    }

    template <typename G = void> requires (T::domain_v == domain::UNIX)
    static unix_address::peer_info_t expected_peer() {
      ucred creds {
        getpid(),
        getuid(),
        getgid(),
      };
      std::ifstream cmdline_file("/proc/self/cmdline");
      std::string cmdline;
      std::getline(cmdline_file, cmdline, '\0');

      auto* pwuid = ::getpwuid(creds.uid);
      std::string username(pwuid->pw_name);

      auto* grgid = ::getgrgid(creds.gid);
      std::string groupname(grgid->gr_name);

      return unix_address::peer_info_t
        ( test_addr().addr()
        , creds
        , cmdline
        , username
        , groupname
        );
    }

    void open_server() {
      auto res = server.open({test_addr()});
      ASSERT_RESULT_OK(res);
    }

    void open_client() {
      auto res = client.open({test_addr()});
      ASSERT_RESULT_OK(res);
    }
};

template <typename T>
class single_thread_test_suite : public testing::Test {
  public:

    posix::net::socket
      < side::SERVER
      , T::domain_v
      , type::STREAM
      , policy::single_thread
      > server;
    posix::net::socket
      < side::CLIENT
      , T::domain_v
      > client;

    template <typename G = void> requires (T::domain_v == domain::IPV4)
    static ipv4_address test_addr() {
      return ipv4_address("127.0.0.1", 5555);
    }

    template <typename G = void> requires (T::domain_v == domain::IPV6)
    static ipv6_address test_addr() {
      return ipv6_address("::1", 5556);
    }

    template <typename G = void> requires (T::domain_v == domain::UNIX)
    static unix_address test_addr() {
      return unix_address("/tmp/cpp_stream_socket_test_st.socket");
    }

    void open_server() {
      auto res = server.open({test_addr()});
      ASSERT_RESULT_OK(res);

      auto res_opt = server.set_option(SO_LINGER, linger{1, 0});
      ASSERT_RESULT_OK(res_opt);
    }

    void open_client() {
      auto res = client.open({test_addr()});
      ASSERT_RESULT_OK(res);
    }
};

using TestTypes = ::testing::Types
  < test_params<domain::IPV4>
  , test_params<domain::IPV6>
  , test_params<domain::UNIX>
  >;

TYPED_TEST_SUITE(multithread_test_suite, TestTypes);
TYPED_TEST_SUITE(single_thread_test_suite, TestTypes);

#endif /* STREAM_SOCKET_TEST_SUITE_H */
