#include <atomic>
#include <algorithm>
#include <semaphore>

#include <sys/socket.h>
#include <sys/un.h>

#include <gtest/gtest.h>

#include <gtest_helpers.hpp>
#include <test_suite.hpp>

#include <cpp_stream_socket/socket.hpp>

template <typename Fixture, typename T>
void open_close_server_test(T &&fixture) {
  fixture->open_server();

  ASSERT_EQ(fixture->server.status(), state::ACTIVE);

  fixture->server.close();

  ASSERT_EQ(fixture->server.status(), state::INVALID);
}

template <typename Fixture, typename T>
void get_address_test(T &&fixture) {
  START_SOCKETS(fixture);

  ASSERT_EQ(fixture->server.address(), fixture->test_addr());
  ASSERT_EQ(fixture->client.address(), fixture->test_addr());
}

template <typename Fixture, typename T>
void connection_handler_test(T &&fixture) {
  const std::string data1   = "abcdef";
  const std::string data2   = "123sdkdsakjsd";

  auto test_connection = [&](auto &&conn) {
    if (!conn) return;

    std::string res;
    *conn >> res;
    ASSERT_EQ(res, data1);

    *conn << data2 << "\n" << std::flush;
  };

  fixture->server.on_connected(test_connection);

  START_SOCKETS(fixture);

  fixture->client << data1 << "\n" << std::flush;

  std::string res;
  fixture->client >> res;
  ASSERT_EQ(res, data2);
}

template <typename Fixture, typename T>
void multiple_connections_test(T &&fixture) {
  constexpr int data1 = 7;
  constexpr int data2 = 5;

  using client_t = decltype(fixture->client);

  client_t client2;

  std::atomic<int> gres{0};
  std::counting_semaphore sem{0};

  auto test_connection = [&](auto &&conn) {
    if (!conn) return;

    int res;
    *conn >> res;
    gres += res;
    *conn << data2 << "\n" << std::flush;
    sem.release();
  };

  fixture->server.on_connected(test_connection);

  START_SOCKETS(fixture);

  auto res_client = client2.open({Fixture::test_addr()});
  ASSERT_RESULT_OK(res_client);

  fixture->client << data1 << "\n" << std::flush;
  client2 << data2 << "\n" << std::flush;

  sem.acquire();
  sem.acquire();
  ASSERT_EQ(gres, data1 + data2);

  int res;
  fixture->client >> res;
  ASSERT_EQ(res, data2);

  res = 0;
  client2 >> res;
  ASSERT_EQ(res, data2);
}

template <domain domain_v, typename addr_t = detect_address<domain_v>>
void test_server_from_fd(void* raw_addr, socklen_t len, addr_t addr) {
  auto fd = ::socket(std::to_underlying(domain_v), SOCK_STREAM, 0);

  ::bind(fd, reinterpret_cast<const sockaddr*>(raw_addr), len);
  ::listen(fd, 10);

  posix::net::socket<side::SERVER, domain_v> server;
  auto server_res = server.open(fd);
  ASSERT_RESULT_OK(server_res);

  posix::net::socket<side::CLIENT, domain_v> client;
  auto client_res = client.open({addr});
  ASSERT_RESULT_OK(client_res);

  const std::string data("ajsndfjksndjf");
  std::string res;

  client << data << std::endl << std::flush;
  wait_for_data(server);
  std::getline(server, res);

  ASSERT_EQ(res, data);
}

template <typename Fixture, typename T>
void peer_info_test(T &&fixture) {
  auto test_connection = [&](auto &&conn) {
    if (!conn) return;

    [[maybe_unused]] auto _ = conn->get();
    auto peer_info = conn->template peer_info<decltype(fixture->server.address())>();
    ASSERT_RESULT_OK(peer_info);
  };

  fixture->server.on_connected(test_connection);

  START_SOCKETS(fixture);

  fixture->client << "0\n" << std::flush;
  wait_for_data(fixture->server);

  auto client_peer_info = fixture->client.peer_info();
  ASSERT_RESULT_OK(client_peer_info);

  ASSERT_TRUE(fixture->expected_peer() == *client_peer_info);
}

TEST(common, error_codes) {
  for (uint8_t i = 0; i <= std::to_underlying(error_code::UNKNOWN_ERROR); ++i) {
    posix::net::error e{error_code(i)};

    ASSERT_FALSE(e.reason().empty());
    ASSERT_FALSE(e.msg().empty());
  }
}

TEST(common, create_server_socket_from_fd_unix) {
  const std::string addr_str("/tmp/test_server_socket_from_fd");
  sockaddr_un addr = {0,{0,},};
  addr.sun_family = AF_UNIX;
  std::copy(addr_str.begin(), addr_str.end(), addr.sun_path);

  test_server_from_fd<domain::UNIX>(&addr, sizeof(addr), unix_address(addr_str));
}

TEST(common, create_server_socket_from_fd_ipv4) {
  const std::string addr_str("127.0.0.1");
  constexpr int port = 6663;
  char str[INET_ADDRSTRLEN];

  sockaddr_in addr = {0,0,{0,},{0,},};
  addr.sin_family = AF_INET;
  addr.sin_port = ::htons(port);
  inet_pton(AF_INET, addr_str.c_str(), &addr.sin_addr.s_addr);
  inet_ntop(AF_INET, &addr.sin_addr, str, INET_ADDRSTRLEN);

  test_server_from_fd<domain::IPV4>(&addr, sizeof(addr), ipv4_address(addr_str, port));
}

TEST(common, create_server_socket_from_fd_ipv6) {
  const std::string addr_str("::1");
  constexpr int port = 6664;

  sockaddr_in6 addr = {0,0,0,{{{0}},},0};
  addr.sin6_family = AF_INET6;
  addr.sin6_port = ::htons(port);
  inet_pton(AF_INET6, addr_str.c_str(), &addr.sin6_addr.s6_addr);

  test_server_from_fd<domain::IPV6>(&addr, sizeof(addr), ipv6_address(addr_str, port));
}

/************************ Multithread policy tests **************************/
TYPED_TEST(multithread_test_suite, open_close_server)    {open_close_server_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, connection_handler)   {connection_handler_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, multiple_connections) {multiple_connections_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, get_address)          {get_address_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, peer_info)            {peer_info_test<TestFixture>(this);}

/*************************** Single thread policy tests *********************/
TYPED_TEST(single_thread_test_suite, open_close_server)     {open_close_server_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, connection_handler)    {connection_handler_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, multiple_connections)  {multiple_connections_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, get_address)           {get_address_test<TestFixture>(this);}
