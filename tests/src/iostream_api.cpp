#include <gtest/gtest.h>

#include <cstdint>
#include <thread>
#include <chrono>
#include <semaphore>

#include <gtest_helpers.hpp>
#include <test_suite.hpp>

#include <cpp_stream_socket/socket.hpp>

template <typename TestFixture, typename T>
void get_one_byte_test(T &&fixture) {
  START_SOCKETS(fixture);

  uint8_t a = 10;
  uint8_t b = 20;

  fixture->client << 0 << std::flush;
  wait_for_data(fixture->server);
  fixture->server << a << std::flush;
  b = fixture->client.get();

  EXPECT_EQ(a, b);
}

template <typename TestFixture, typename T>
void get_multiple_bytes_test(T &&fixture) {
  START_SOCKETS(fixture);

  std::string a = "abrfssfds";
  std::string b;

  fixture->client << 0 << std::flush;
  wait_for_data(fixture->server);
  fixture->server << a << std::flush;

  for (size_t i = 0; i < a.size(); ++i) {
    b.push_back(fixture->client.get());
  }

  EXPECT_EQ(a, b);
}

template <typename TestFixture, typename T>
void read_string_test(T &&fixture) {
  const std::string a = "asscdvsd";
  const std::string b = "sndkdnfvkdnDSUYC12312v";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    std::string res;
    *conn >> res;
    ASSERT_EQ(res, a);

    *conn << b << std::endl;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::endl;

  std::string res;
  fixture->client >> res;
  ASSERT_EQ(res, b);
}

template <typename TestFixture, typename T>
void read_strings_test(T &&fixture) {
  const std::string a = "asscdvsd";
  const std::string b = "sndkdnfvkdnDSUYC12312v";
  const std::string c = "jnssvnJNDCnjksd9";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    std::string x, y, z;

    *conn >> x;
    *conn >> y;
    *conn >> z;

    ASSERT_EQ(x, a);
    ASSERT_EQ(y, b);
    ASSERT_EQ(z, c);

    *conn << b << std::endl;
    *conn << a << std::endl;
    *conn << c << std::endl;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::endl;
  fixture->client << b << std::endl;
  fixture->client << c << std::endl;

  std::string x, y, z;
  fixture->client >> x;
  fixture->client >> y;
  fixture->client >> z;

  ASSERT_EQ(b, x);
  ASSERT_EQ(a, y);
  ASSERT_EQ(c, z);
}

template <typename TestFixture, typename T>
void read_int_test(T &&fixture) {
  constexpr int a = 42;
  constexpr int b = -73234;
  constexpr int c = INT_MAX;
  constexpr int d = INT_MIN;

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    int x, y, z, t;
    *conn >> x;
    *conn >> y;
    *conn >> z;
    *conn >> t;

    ASSERT_EQ(x, a);
    ASSERT_EQ(y, b);
    ASSERT_EQ(z, c);
    ASSERT_EQ(t, d);

    *conn << b << std::endl;
    *conn << d << std::endl;
    *conn << c << std::endl;
    *conn << a << std::endl;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::endl;
  fixture->client << b << std::endl;
  fixture->client << c << std::endl;
  fixture->client << d << std::endl;

  int x, y, z, t;

  fixture->client >> x;
  fixture->client >> y;
  fixture->client >> z;
  fixture->client >> t;

  ASSERT_EQ(x, b);
  ASSERT_EQ(y, d);
  ASSERT_EQ(z, c);
  ASSERT_EQ(t, a);
}

template <typename TestFixture, typename T>
void peek_test(T &&fixture) {
  const std::string a = "dfjnsdkcnkn23e32";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    std::string res;

    for (int i = 0; i < 1000; ++i) res += conn->peek();
    ASSERT_EQ(res, std::string(1000, 'd'));

    *conn >> res;
    ASSERT_EQ(res, a);
    *conn << a << std::endl;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::endl;

  std::string res;
  for (int i = 0; i < 1000; ++i) res += fixture->client.peek();
  ASSERT_EQ(res, std::string(1000, 'd'));
  fixture->client >> res;
  ASSERT_EQ(res, a);
}

template <typename TestFixture, typename T>
void unget_and_clear_test(T &&fixture) {
  const std::string a = "ksdfnvkdsvndv";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    std::string res;
    *conn >> res;

    EXPECT_TRUE(conn->good());
    conn->unget();
    EXPECT_FALSE(conn->good());
    conn->clear();
    EXPECT_TRUE(conn->good());

    ASSERT_EQ(res, a);
    *conn << a << std::endl;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::endl;

  std::string res;
  fixture->client >> res;
  EXPECT_TRUE(fixture->client.good());
  fixture->client.unget();
  EXPECT_FALSE(fixture->client.good());
  fixture->client.clear();
  EXPECT_TRUE(fixture->client.good());

  ASSERT_EQ(res, a);
}

template <typename TestFixture, typename T>
void put_back_test(T &&fixture) {
  constexpr uint8_t a = 42;
  constexpr uint8_t b = 76;

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    EXPECT_TRUE(conn->good());

    uint8_t x = 0;
    x = conn->get();
    EXPECT_EQ(x, a);

    conn->putback(11);
    EXPECT_TRUE(conn->good());

    x = conn->get();
    EXPECT_EQ(x, 11);

    x = conn->get();
    EXPECT_EQ(x, b);

    EXPECT_TRUE(conn->good());

    *conn << a << b << std::flush;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << b << std::flush;

  uint8_t x;
  x = fixture->client.get();
  ASSERT_EQ(x, a);

  fixture->client.putback(123);

  x = fixture->client.get();
  ASSERT_EQ(x, 123);

  x = fixture->client.get();
  ASSERT_EQ(x, b);

  ASSERT_TRUE(fixture->client.good());
}

template <typename TestFixture, typename T>
void ignore_test(T &&fixture) {
  const std::string a = "kjsnvjkdnfvkjfnbkfdv#";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    std::string x;
    conn->ignore(7, '#');
    *conn >> x;
    ASSERT_EQ(x, std::string(a, 7));

    *conn << a << std::endl;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::endl;

  std::string x;
  fixture->client.ignore(5, '#');
  fixture->client >> x;
  ASSERT_EQ(x, std::string(a, 5));
}

template <typename TestFixture, typename T>
void read_test(T &&fixture) {
  const std::string a = "32rwiojekrfsfdcBSCDMHJCB7";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    char x[5];
    conn->read(x, 5);
    ASSERT_EQ(std::string(x, 5), std::string(a, 0, 5));
    conn->read(x, 3);
    ASSERT_EQ(std::string(x, 3), std::string(a, 5, 3));
    conn->read(x, 1);
    ASSERT_EQ(std::string(x, 1), std::string(a, 8, 1));
    conn->read(x, 0);
    ASSERT_EQ(std::string(x, 0), std::string(a, 9, 0));
    conn->read(x, 4);
    ASSERT_EQ(std::string(x, 4), std::string(a, 9, 4));

    *conn << a << std::endl;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::flush;

  char x[5];
  fixture->client.read(x, 5);
  ASSERT_EQ(std::string(x, 5), std::string(a, 0, 5));
  fixture->client.read(x, 3);
  ASSERT_EQ(std::string(x, 3), std::string(a, 5, 3));
  fixture->client.read(x, 1);
  ASSERT_EQ(std::string(x, 1), std::string(a, 8, 1));
  fixture->client.read(x, 0);
  ASSERT_EQ(std::string(x, 0), std::string(a, 9, 0));
  fixture->client.read(x, 4);
  ASSERT_EQ(std::string(x, 4), std::string(a, 9, 4));
}

template <typename TestFixture, typename T>
void read_some_test(T &&fixture) {
  const std::string a = "abc";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    char x[5];
    [[maybe_unused]] auto _ = conn->peek();

    auto n = conn->readsome(x, 5);
    ASSERT_EQ(a, std::string(x, n));

    *conn << a << std::flush;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::flush;

  char x[5];
  [[maybe_unused]] auto _ = fixture->client.peek();

  auto n = fixture->client.readsome(x, 5);
  ASSERT_EQ(a, std::string(x, n));
}

template <typename TestFixture, typename T>
void gcount_test(T &&fixture) {
  const std::string a = "kjsfnvdsknsli8UHaiujhnczK";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    char x[5];
    char y[100];

    ASSERT_EQ(conn->gcount(), 0);
    conn->read(x, 5);
    ASSERT_EQ(conn->gcount(), 5);
    ASSERT_EQ(std::string(x, 5), std::string(a, 0, 5));

    conn->getline(y, 100);
    ASSERT_EQ(conn->gcount(), a.size() - 5 + 1);
    ASSERT_EQ(y, std::string(a, 5));

    conn->putback('a');
    ASSERT_EQ(conn->gcount(), 0);

    *conn << a << std::endl;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::endl;

  char x[5];
  char y[100];

  ASSERT_EQ(fixture->client.gcount(), 0);
  fixture->client.read(x, 5);
  ASSERT_EQ(fixture->client.gcount(), 5);
  ASSERT_EQ(std::string(x, 5), std::string(a, 0, 5));

  fixture->client.getline(y, 100);
  ASSERT_EQ(fixture->client.gcount(), a.size() - 5 + 1);
  ASSERT_EQ(y, std::string(a, 5));

  fixture->client.putback('a');
  ASSERT_EQ(fixture->client.gcount(), 0);
}

template <typename TestFixture, typename T>
void tellg_test(T &&fixture) {
  const std::string a = "Hello, world!";

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    std::string x;
    *conn >> x;
    ASSERT_EQ(x, "Hello,");
    ASSERT_EQ(conn->tellg(), -1);
    *conn << a << std::flush;
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  fixture->client << a << std::flush;

  std::string x;
  fixture->client >> x;
  ASSERT_EQ(x, "Hello,");
  ASSERT_EQ(fixture->client.tellg(), -1);
}

template <typename TestFixture, typename T>
void seekg_test(T &&fixture) {
  const std::string a = "Hello, world!";
  std::binary_semaphore sem{0};

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    sem.release();
    *conn << a << std::endl;

    std::string x;
    *conn >> x;
    ASSERT_EQ(x, "Hello,");
    conn->seekg(0);
    ASSERT_FALSE(conn->good());

    x.clear();
    *conn >> x;
    ASSERT_EQ(x, "");
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  sem.acquire();
  fixture->client << a << std::endl;

  std::string x;
  fixture->client >> x;
  ASSERT_EQ(x, "Hello,");
  fixture->client.seekg(0);
  ASSERT_FALSE(fixture->client.good());

  x.clear();
  fixture->client >> x;
  ASSERT_EQ(x, "");
}

template <typename TestFixture, typename T>
void put_test(T &&fixture) {
  std::binary_semaphore sem{0};

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    sem.release();
    char x = 0;
    x = conn->get();
    ASSERT_EQ(x, 1);
    x = conn->get();
    ASSERT_EQ(x, 2);
    x = conn->get();
    ASSERT_EQ(x, 3);
    x = conn->get();
    ASSERT_EQ(x, 4);

    conn->put(1).put(2).put(3).put(4).flush();
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  sem.acquire();
  fixture->client.put(1).put(2).put(3).put(4).flush();

  char x = 0;
  x = fixture->client.get();
  ASSERT_EQ(x, 1);
  x = fixture->client.get();
  ASSERT_EQ(x, 2);
  x = fixture->client.get();
  ASSERT_EQ(x, 3);
  x = fixture->client.get();
  ASSERT_EQ(x, 4);
}

template <typename TestFixture, typename T>
void write_test(T &&fixture) {
  std::binary_semaphore sem{0};

  auto conn_handler = [&](auto &&conn) {
    if (!conn) {
      FAIL() << "Connection failed";
      return;
    }

    sem.release();
    char a[10] = "123456789";
    char x[10] = {0,};

    conn->write(a, 10).flush();
    conn->read(x, 10);

    ASSERT_EQ(std::string(x, 10), std::string(a, 10));
  };

  fixture->server.on_connected(conn_handler);
  START_SOCKETS(fixture);

  char a[10] = "123456789";
  char x[10] = {0,};

  sem.acquire();
  fixture->client.write(a, 10).flush();
  fixture->client.read(x, 10);

  ASSERT_EQ(std::string(x, 10), std::string(a, 10));
}

template <typename TestFixture, typename T>
void tellp_test(T &&fixture) {
  const std::string a = "Hello world";

  START_SOCKETS(fixture);

  fixture->client << a << std::flush;
  fixture->server << a << std::flush;
  EXPECT_EQ(fixture->client.tellp(), -1);
  EXPECT_EQ(fixture->server.tellp(), -1);
}

template <typename TestFixture, typename T>
void seekp_test(T &&fixture) {
  const std::string a = "Hello world";

  START_SOCKETS(fixture);

  fixture->server.seekp(0, std::ios_base::end);
  fixture->client.seekp(0, std::ios_base::end);
  EXPECT_TRUE(fixture->server.fail());
  EXPECT_TRUE(fixture->client.fail());
}

/************************* Multithread policy tests *************************/
TYPED_TEST(multithread_test_suite, get_one_byte)       {get_one_byte_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, get_multiple_bytes) {get_multiple_bytes_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, read_string)        {read_string_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, read_strings)       {read_strings_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, read_int)           {read_int_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, peek)               {peek_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, unget_and_clear)    {unget_and_clear_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, put_back)           {put_back_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, ignore)             {ignore_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, read)               {read_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, read_some)          {read_some_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, gcount)             {gcount_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, tellg)              {tellg_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, seekg)              {seekg_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, put)                {put_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, write)              {write_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, tellp)              {tellp_test<TestFixture>(this);}
TYPED_TEST(multithread_test_suite, seekp)              {seekp_test<TestFixture>(this);}

/*********************** Single thread policy tests *************************/
TYPED_TEST(single_thread_test_suite, read_string)     {read_string_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, read_strings)    {read_strings_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, read_int)        {read_int_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, peek)            {peek_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, unget_and_clear) {unget_and_clear_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, put_back)        {put_back_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, ignore)          {ignore_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, read)            {read_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, read_some)       {read_some_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, gcount)          {gcount_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, tellg)           {tellg_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, seekg)           {seekg_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, put)             {put_test<TestFixture>(this);}
TYPED_TEST(single_thread_test_suite, write)           {write_test<TestFixture>(this);}
