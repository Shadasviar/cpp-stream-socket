#include <gtest/gtest.h>

#include <chrono>

#include <gtest_helpers.hpp>

#include <cpp_stream_socket/socket.hpp>

TEST(performance, close_time) {
  using namespace posix::net;
  using namespace std::chrono;
  using namespace std::chrono_literals;

  posix::net::socket<side::SERVER, domain::IPV4> s;

  constexpr int n_allowed_fail = 2000;
  int failed_sockets = 0;
  for (int i = 0; i < 10000 + n_allowed_fail; ++i) {
    auto res = s.open({{"127.0.0.1", 10000 + i}});
    if (!res) {
      ++failed_sockets;
      continue;
    }
    ASSERT_RESULT_OK(res);

    auto start = steady_clock::now();
    s.close();
    auto stop = steady_clock::now();
    auto close_time = duration_cast<milliseconds>(stop - start);

    ASSERT_EQ(s.status(), state::INVALID);
    ASSERT_LT(close_time, 10ms);
  }

  ASSERT_LT(failed_sockets, n_allowed_fail);
}

TEST(performance, mt_connections_closing_joinable) {
  using namespace posix::net;
  using namespace std::chrono_literals;

  auto conn_handler = [] (auto &&conn) {
    if (!conn) return;
  };

  posix::net::socket<side::SERVER, domain::IPV4> s;
  s.on_connected(conn_handler);
  auto server_res = s.open({{"127.0.0.1", 5555}});
  ASSERT_RESULT_OK(server_res);

  for (int i = 0; i < 100; ++i) {
    posix::net::socket<side::CLIENT, domain::IPV4> c;
    auto client_res = c.open({{"127.0.0.1", 5555}});
    ASSERT_RESULT_OK(client_res);
    c.close();
  }

  int n_retry = 100;
  while (s.n_connections() || n_retry--) std::this_thread::yield();
  ASSERT_EQ(s.n_connections(), 0);
}

TEST(performance, mt_connections_closing_hup) {
  using namespace posix::net;
  using namespace std::chrono_literals;

  auto conn_handler = [] (auto &&conn) {
    if (!conn) return;
    [[maybe_unused]] auto _ = conn->get();
  };

  posix::net::socket<side::SERVER, domain::IPV4> s;
  s.on_connected(conn_handler);
  auto server_res = s.open({{"127.0.0.1", 5555}});
  ASSERT_RESULT_OK(server_res);

  for (int i = 0; i < 100; ++i) {
    posix::net::socket<side::CLIENT, domain::IPV4> c;
    auto client_res = c.open({{"127.0.0.1", 5555}});
    ASSERT_RESULT_OK(client_res);
    c.close();
  }

  int n_retry = 100;
  while (s.n_connections() || n_retry--) std::this_thread::yield();
  ASSERT_EQ(s.n_connections(), 0);
}
