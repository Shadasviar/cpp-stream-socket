#include <cpp_stream_socket/address/ipv4.hpp>

#include <stdexcept>

#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net {

  ipv4_address::ipv4_address(std::string_view ip, int port)
    : _str_addr(ip)
    , _port(port) {
  }

  ipv4_address::ipv4_address(const sockaddr* addr, socklen_t) {
    auto addr_in = reinterpret_cast<const sockaddr_in*>(addr);
    if (addr_in->sin_family != AF_INET)
      throw std::runtime_error("Invalid address type");

    _port = ::ntohs(addr_in->sin_port);

    char str[INET_ADDRSTRLEN];
    auto ret = ::inet_ntop(AF_INET, &addr_in->sin_addr, str, INET_ADDRSTRLEN);
    if (!ret)
      throw std::runtime_error("Invalid address format");

    _str_addr = std::string(str);
  }

  void ipv4_address::close() {
  }

  std::string_view ipv4_address::addr() const {
    return _str_addr;
  }

  in_port_t ipv4_address::port() const {
    return _port;
  }

  result<ipv4_address::peer_info_t> ipv4_address::peer_info(int fd) {
    address raw;
    raw.length = sizeof(raw.raw_addr);

    auto ret = ::getpeername(fd, reinterpret_cast<sockaddr*>(&raw.raw_addr), &raw.length);
    if (ret < 0) return make_error(error_code::GET_PEER_INFO_FAILED);

    return peer_info_t(reinterpret_cast<sockaddr*>(&raw.raw_addr), raw.length);
  }

  result<address> ipv4_address::posix_addr() const {
    union {sockaddr_in in; sockaddr_storage s;} addr {{
      AF_INET,
      ::htons(_port),
      {0},{0},
    }};

    auto ret = inet_pton(AF_INET, _str_addr.c_str(), &addr.in.sin_addr.s_addr);
    if (ret != 1) return make_error(error_code::INVALID_ADDRESS);

    return address{addr.s, sizeof(addr)};
  }

} /* namespace posix::net */
