#include <cpp_stream_socket/address/unix.hpp>

#include <algorithm>
#include <cstdio>
#include <stdexcept>
#include <cstddef>

#include <cpp_stream_socket/exit_code.hpp>

#include <sys/un.h>

namespace posix::net {

  unix_address::unix_address(std::string_view path)
    : _str_addr(path) {
  }

  unix_address::unix_address(const sockaddr* addr, socklen_t len) {
    auto addr_un = reinterpret_cast<const sockaddr_un*>(addr);
    if (addr_un->sun_family != AF_UNIX)
      throw std::runtime_error("Invalid address family");

    _str_addr = std::string(addr_un->sun_path, len);
  }

  result<unix_address::peer_info_t> unix_address::peer_info(int fd) {
    return make_unix_peer_info(fd);
  }

  void unix_address::close() {
    if (!_str_addr.empty()) {
      std::remove(_str_addr.c_str());
    }
  }

  std::string_view unix_address::addr() const {
    return _str_addr;
  }

  result<address> unix_address::posix_addr() const {
    constexpr size_t max_len = 107;

    union {sockaddr_un un; sockaddr_storage s;} addr {{
      AF_UNIX,
      {0},
    }};

    if (_str_addr.size() > max_len) {
      return make_error
        ( error_code::INVALID_ADDRESS
        , std::runtime_error("Address is too long")
        );
    }

    std::ranges::copy(_str_addr, addr.un.sun_path);
    addr.un.sun_path[_str_addr.size()] = '\0';

    return address
      { addr.s
      , static_cast<socklen_t>
        ( offsetof(sockaddr_un, sun_path)
        + _str_addr.size()
        + 1
        )
      };
  }

} /* namespace posix::net */
