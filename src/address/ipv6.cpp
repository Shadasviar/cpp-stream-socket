#include <cpp_stream_socket/address/ipv6.hpp>

#include <stdexcept>

#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net {

  ipv6_address::ipv6_address(std::string_view ip, int port)
    : _str_addr(ip)
    , _port(port) {
  }

  ipv6_address::ipv6_address(const sockaddr* addr, socklen_t) {
    auto addr_in6 = reinterpret_cast<const sockaddr_in6*>(addr);
    if (addr_in6->sin6_family != AF_INET6)
      throw std::runtime_error("Invalid address type");

    _port = ::ntohs(addr_in6->sin6_port);

    char str[INET6_ADDRSTRLEN];
    auto ret = ::inet_ntop(AF_INET6, &addr_in6->sin6_addr, str, INET6_ADDRSTRLEN);
    if (!ret)
      throw std::runtime_error("Invalid address format");

    _str_addr = std::string(str);
  }

  result<ipv6_address::peer_info_t> ipv6_address::peer_info(int fd) {
    address raw;
    raw.length = sizeof(raw.raw_addr);

    auto ret = ::getpeername(fd, reinterpret_cast<sockaddr*>(&raw.raw_addr), &raw.length);
    if (ret < 0) return make_error(error_code::GET_PEER_INFO_FAILED);

    return peer_info_t(reinterpret_cast<sockaddr*>(&raw.raw_addr), raw.length);
  }

  void ipv6_address::close() {
  }

  std::string_view ipv6_address::addr() const {
    return _str_addr;
  }

  in_port_t ipv6_address::port() const {
    return _port;
  }

  result<address> ipv6_address::posix_addr() const {
    union {sockaddr_in6 in; sockaddr_storage s;} addr {{
      .sin6_family    = AF_INET6,
      .sin6_port      = ::htons(_port),
      .sin6_flowinfo  = 0,
      .sin6_addr      = {{{0},}},
      .sin6_scope_id  = 0,
    }};

    auto ret = inet_pton(AF_INET6, _str_addr.c_str(), &addr.in.sin6_addr.s6_addr);
    if (ret != 1) return make_error(error_code::INVALID_ADDRESS);

    return address{addr.s, sizeof(addr)};
  }

} /* namespace posix::net */

