#include <cpp_stream_socket/address/unix_peer_info.hpp>

#include <fstream>

#include <sys/un.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

namespace posix::net {

  bool operator==(const unix_peer_info &lhs, const unix_peer_info &rhs) {
    return lhs.addr() == rhs.addr()
        && lhs.pid() == rhs.pid()
        && lhs.gid() == rhs.gid()
        && lhs.uid() == rhs.uid()
        && lhs.progname() == rhs.progname()
        && lhs.username() == rhs.username()
        && lhs.groupname() == rhs.groupname()
        ;
  }

  unix_peer_info::unix_peer_info
    ( std::string_view path
    , const ucred &creds
    , std::string_view progname
    , std::string_view username
    , std::string_view groupname
    )
    : _str_addr(path)
    , _creds(creds)
    , _progname(progname)
    , _username(username)
    , _groupname(groupname)
  {}

  result<unix_peer_info> make_unix_peer_info(int fd) {
    sockaddr_un addr;
    socklen_t len = sizeof(addr);

    auto ret = ::getpeername(fd, reinterpret_cast<sockaddr*>(&addr), &len);
    if (ret < 0) return make_error(error_code::GET_PEER_INFO_FAILED);
    std::string str_addr(addr.sun_path);

    ucred creds;
    len = sizeof(creds);
    ret = ::getsockopt(fd, SOL_SOCKET, SO_PEERCRED, &creds, &len);
    if (ret < 0) return make_error(error_code::GET_PEER_CREDS_FAILED);

    std::ifstream cmdline_file("/proc/" + std::to_string(creds.pid) + "/cmdline");
    std::string cmdline;
    std::getline(cmdline_file, cmdline, '\0');

    auto* pwuid = ::getpwuid(creds.uid);
    if (!pwuid) return make_error(error_code::GET_PEER_CREDS_FAILED);
    std::string username(pwuid->pw_name);

    auto* grgid = ::getgrgid(creds.gid);
    if (!grgid) return make_error(error_code::GET_PEER_CREDS_FAILED);
    std::string groupname(grgid->gr_name);

    return unix_peer_info(str_addr, creds, cmdline, username, groupname);
  }

  std::string_view unix_peer_info::addr() const {
    return _str_addr;
  }

  std::string_view unix_peer_info::progname() const {
    return _progname;
  }

  std::string_view unix_peer_info::username() const {
    return _username;
  }

  std::string_view unix_peer_info::groupname() const {
    return _groupname;
  }

  pid_t unix_peer_info::pid() const {
    return _creds.pid;
  }

  uid_t unix_peer_info::uid() const {
    return _creds.uid;
  }

  gid_t unix_peer_info::gid() const {
    return _creds.gid;
  }

} /* namespace posix::net */
