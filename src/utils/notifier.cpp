#include <cpp_stream_socket/utils/notifier.hpp>

#include <utility>

#include <sys/socket.h>
#include <unistd.h>

namespace posix::net {

  notifier::~notifier() {
    close();
  }

  notifier& notifier::operator=(notifier &&rhs) {
    if (&rhs == this) return *this;

    close();
    _fds = rhs._fds;
    _state_is_valid.exchange(rhs._state_is_valid.load());
    rhs._state_is_valid = false;
    return *this;
  }

  notifier::notifier(notifier &&rhs) {
    *this = std::move(rhs);
  }

  void notifier::close() noexcept {
    if (_state_is_valid.load()) {
      _state_is_valid = false;
      for (int fd : _fds) ::close(fd);
    }
  }

  result<notifier> notifier::create() noexcept {
    try {
      notifier res;
      auto err = ::socketpair(AF_UNIX, SOCK_DGRAM, 0, res._fds.data());
      if (err < 0) return make_error(error_code::NOTIFIER_FAILED);
      res._state_is_valid = true;
      return res;
    } catch (const std::exception &e) {
      return make_error(error_code::NOTIFIER_FAILED, e);
    }
  }

  int notifier::get_fd(notifier::type t) noexcept {
    return _fds[std::to_underlying(t)];
  }

  result<void> notifier::notify() noexcept {
    uint8_t x = 1;
    auto ret = ::write(_fds[std::to_underlying(type::NOTIFIER)], &x, sizeof(x));
    if (ret < 0) return make_error(error_code::NOTIFIER_FAILED);
    return {};
  }

} /* namespace posix::net */
