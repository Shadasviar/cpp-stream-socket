#include <cpp_stream_socket/utils/poll.hpp>

#include <numeric>

namespace posix::net::detail {

  pollfds::pollfds(int timeout, const pollfds::events_for_fd &expected_events)
    : _timeout(timeout)
    {
    auto bin_or = [](auto &&a, auto &&b){return a | b;};

    for (auto &[fd, es] : expected_events) {
      auto events = std::reduce(es.begin(), es.end(), 0, bin_or);

      _pfds.emplace_back(fd, events, 0);
      _expected_events[fd] = events;
    }
  }

  result<pollfds::triggered_fds> pollfds::operator()() {

    auto ret = ::poll(_pfds.data(), _pfds.size(), _timeout);
    if (ret < 0)  return make_error(error_code::POLL_FAILED);
    if (ret == 0) return {};

    triggered_fds res;
    for (auto &pfd : _pfds) {
      if
        ( _expected_events.contains(pfd.fd)
        && (pfd.revents & _expected_events.at(pfd.fd))
        ) {
        res.insert(pfd.fd);
      }
    }
    return res;
  }

} /* namespace posix::net::detail */
