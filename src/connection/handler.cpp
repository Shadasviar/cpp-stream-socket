#include <cpp_stream_socket/connection/handler.hpp>

namespace posix::net::detail {

  void connection_handler::set_handler(const connection::handler &h) {
    _handler = h;
  }

  void connection_handler::invoke(result<connection> &&conn) {
    if (!_handler) return;

    _handler(std::forward<decltype(conn)>(conn));
  }

} /* namespace posix::net::detail */
