#include <cpp_stream_socket/connection/connection.hpp>

#include <unistd.h>
#include <sys/ioctl.h>

#include <algorithm>

#include <cpp_stream_socket/utils/poll.hpp>
#include <cpp_stream_socket/utils/functional.hpp>
#include <cpp_stream_socket/config.hpp>

namespace posix::net {

  static constexpr size_t fallback_read_size  = 1e3;

  connection::connection() : std::iostream(this) {}

  int connection::fd() const noexcept {
    return _fd;
  }

  result<connection> connection::create(int fd) noexcept {
    try {
      return notifier::create()
        .and_then(std::bind_front(&connection::init, fd))
        ;
    } catch (const std::exception &e) {
      return make_error(error_code::CONNECTION_FAILED, e);
    }
  }

  result<connection> connection::init(int fd, notifier &&exit_event) {
    /* Invalid fd means that previous operation failed */
    if (fd < 0) return make_error(error_code::ACCEPT_FAILED);

    auto res = result<connection>();
    res->_fd = fd;
    res->_is_open = true;
    res->_exit_event = std::move(exit_event);
    return res;
  }

  connection::connection(connection &&rhs) : std::iostream(this) {
    *this = std::move(rhs);
  }

  connection& connection::operator=(connection &&rhs) {
    if (&rhs == this) return *this;

    close();

    std::streambuf::operator=(std::move(rhs));
    std::iostream::operator=(std::move(rhs));
    rdbuf(this);

    std::swap(_fd, rhs._fd);
    std::swap(_in_buf, rhs._in_buf);
    std::swap(_out_buf, rhs._out_buf);
    std::swap(_exit_event, rhs._exit_event);
    auto state = _is_open.exchange(rhs._is_open.load());
    rhs._is_open = state;

    return *this;
  }

  connection::~connection() {
    close();
  }

  bool connection::is_open() const noexcept {
    return _is_open.load();
  }

  void connection::close() noexcept {
    if (_is_open.load()) {
      _is_open = false;
      _exit_event.notify();
      ::close(_fd);
    }
  }

  int connection::sync() {
    auto exit = _exit_event.get_fd(notifier::type::RECEIVER);
    auto wait_for_events = detail::pollfds(detail::poll_timeout,
      { {_fd, {POLLOUT, POLLWRBAND}}
      , {exit, {POLLIN, POLLPRI}}
    });

    while (_is_open.load() && _out_buf.size() > 0) {
      auto events = wait_for_events();

      if (!events || events->empty()) continue;
      if (events->contains(exit)) return 0;

      if (events->contains(_fd)) {
        auto written_bytes = ::write(_fd, _out_buf.data(), _out_buf.size());
        if (written_bytes < 0) return -1;
        _out_buf.erase(_out_buf.begin(), _out_buf.begin() + written_bytes);
      }
    }

    return 0;
  }

  int connection::overflow(int c) {
    _out_buf.emplace_back(c);
    return c;
  }

  std::streamsize connection::xsputn(const char* s, std::streamsize n) {
    _out_buf.insert(_out_buf.end(), s, s + n);
    return n;
  }

  int connection::underflow() {
    if (!_in_buf.empty()) return _in_buf.back();

    auto exit = _exit_event.get_fd(notifier::type::RECEIVER);
    auto wait_for_events = detail::pollfds(detail::poll_timeout,
      { {_fd, {POLLIN, POLLPRI}}
      , {exit, {POLLIN, POLLPRI}}
    });

    while (_is_open.load()) {
      auto events = wait_for_events();

      if (!events || events->empty()) continue;
      if (events->contains(exit)) return EOF;

      if (events->contains(_fd)) {
        auto ret = read_all_available_data();
        if (ret < 0) return EOF;
        if (!_in_buf.empty()) return _in_buf.back();
      }
    }

    return EOF;
  }

  int connection::uflow() {
    if (!_in_buf.empty()) {
      char c = _in_buf.back();
      _in_buf.pop_back();
      return static_cast<int>(c);
    }

    auto exit = _exit_event.get_fd(notifier::type::RECEIVER);
    auto wait_for_events = detail::pollfds(detail::poll_timeout,
      { {_fd, {POLLIN, POLLPRI}}
      , {exit, {POLLIN, POLLPRI}}
    });

    while (_is_open.load()) {
      auto events = wait_for_events();

      if (!events || events->empty()) continue;
      if (events->contains(exit)) return EOF;

      if (events->contains(_fd)) {
        auto ret = read_all_available_data();
        if (ret < 0) return EOF;

        if (_in_buf.empty()) continue;
        auto c = _in_buf.back();
        _in_buf.pop_back();
        return c;
      }
    }

    return EOF;
  }

  std::streamsize connection::xsgetn(char* s, std::streamsize n) {
    if (_in_buf.size() >= static_cast<size_t>(n)) {
      std::copy(_in_buf.rbegin(), _in_buf.rbegin() + n, s);
      _in_buf.erase(_in_buf.end() - n, _in_buf.end());
      return n;
    }

    std::streamsize offset = _in_buf.size();

    std::ranges::copy(_in_buf, s);
    _in_buf.clear();

    auto exit = _exit_event.get_fd(notifier::type::RECEIVER);
    auto wait_for_events = detail::pollfds(detail::poll_timeout,
      { {_fd, {POLLIN, POLLPRI}}
      , {exit, {POLLIN, POLLPRI}}
    });

    while (_is_open.load() && offset < n) {
      auto events = wait_for_events();

      if (!events || events->empty()) continue;
      if (events->contains(exit)) return EOF;

      if (events->contains(_fd)) {
        auto ret = read_all_available_data();
        if (ret < 0) return EOF;

        if (_in_buf.size() >= static_cast<size_t>(n) - offset) {
          std::copy(_in_buf.rbegin(), _in_buf.rbegin() + n, s + offset);
          _in_buf.erase(_in_buf.end() - n + offset, _in_buf.end());
          return n;
        }

        offset += _in_buf.size();
        std::ranges::copy(_in_buf, s + offset);
        _in_buf.clear();
      }
    }

    return offset;
  }

  int connection::pbackfail(int c) {
    if (c == EOF) return EOF;
    _in_buf.emplace_back(c);
    return c;
  }

  std::streamsize connection::showmanyc() {
    read_all_available_data();
    return _in_buf.size();
  }

  int connection::read_all_available_data() {
    int bytes_ready = 0;

    auto ret = ::ioctl(_fd, FIONREAD, &bytes_ready);
    if (ret != 0) bytes_ready = fallback_read_size;

    std::vector<char> tmp(bytes_ready);
    auto read_bytes = ::read(_fd, tmp.data(), bytes_ready);
    if (read_bytes < 0) return read_bytes;

    _in_buf.insert(_in_buf.begin(), tmp.rbegin(), tmp.rbegin() + read_bytes);

    return read_bytes;
  }

} /* namespace posix::net */
