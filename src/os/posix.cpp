#include <cpp_stream_socket/os/posix.hpp>

#include <utility>

namespace posix::net {

  [[nodiscard]] result<int> create_socket(domain d, type t) noexcept {
    int fd = ::socket(std::to_underlying(d), std::to_underlying(t), 0);
    if (fd < 0) return make_error(error_code::CREATE_SOCKET_FAILED);
    return fd;
  }

  [[nodiscard]] result<int> bind(const address &addr, int fd) noexcept {
    auto ret = ::bind(fd, addr.ptr(), addr.length);
    if (ret < 0) return make_error(error_code::BIND_FAILED);
    return fd;
  }

  [[nodiscard]] result<int> connect(const address &addr, int fd) noexcept {
    auto ret = ::connect(fd, addr.ptr(), addr.length);
    if (ret < 0) return make_error(error_code::CONNECTION_FAILED);
    return fd;
  }

} /* namespace posix::net */
