#include <cpp_stream_socket/exit_code.hpp>

namespace posix::net {

  namespace detail {
    static const std::unordered_map<error_code, std::string> error_msg {
      {error_code::CREATE_SOCKET_FAILED , "failed to create socket"},
      {error_code::INVALID_ADDRESS      , "invalid socket address"},
      {error_code::BIND_FAILED          , "binding address to socket failed"},
      {error_code::LISTEN_FAILED        , "failed to listen for connections"},
      {error_code::CONNECTION_FAILED    , "failed to connect to server"},
      {error_code::ACCEPT_FAILED        , "failed to accept connection"},
      {error_code::MT_POLICY_FAILED     , "multithread policy failed to start"},
      {error_code::ST_POLICY_FAILED     , "single thread policy failed to start"},
      {error_code::POLL_FAILED          , "polling on file descriptor failed"},
      {error_code::NOTIFIER_FAILED      , "failed to notify another thread"},
      {error_code::SETSOCKOPT_FAILED    , "failed to set socket option"},
      {error_code::GETSOCKOPT_FAILED    , "failed to get socket option"},
      {error_code::FD_IS_NOT_LISTENING  , "given file descriptor is not listening"},
      {error_code::TYPE_MISMATCH        , "socket type does not match to fd type"},
      {error_code::GETSOCKNAME_FAILED   , "failed to get socket address from fd"},
      {error_code::GET_PEER_INFO_FAILED , "failed to get peer name"},
      {error_code::GET_PEER_CREDS_FAILED, "failed to get peer credentials"},
      {error_code::UNKNOWN_ERROR        , "unknown error occured"},
    };
  } /* namespace detail */

  error::error(error_code code) noexcept : _code(code), _reason(errno) {
  }

  error::error(error_code code, const std::exception &e) noexcept
    : _code(code)
    , _reason(e) {
  }

  std::string error::msg() const {
    return detail::error_msg.at(_code);
  }

  std::string error::reason() const {
    return std::visit(reason_visitor(), _reason);
  }

  std::string error::reason_visitor::operator()(int errnum) const {
    return std::string(strerror(errnum));
  }

  std::string error::reason_visitor::operator()(const std::exception &e) const {
    return std::string(e.what());
  }

} /* namespace posix::net */
