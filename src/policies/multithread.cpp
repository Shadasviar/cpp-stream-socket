#include <cpp_stream_socket/policies/multithread.hpp>

#include <functional>
#include <algorithm>

#include <sys/socket.h>
#include <unistd.h>

#include <cpp_stream_socket/utils/functional.hpp>
#include <cpp_stream_socket/utils/poll.hpp>
#include <cpp_stream_socket/config.hpp>

namespace posix::net::policy {

  multithread::~multithread() {
    close();
  }

  void multithread::close() noexcept {
    if (!_stop.load()) {
      _stop = true;
      ::close(_fd);
      _notifier.notify();
      if (_acceptor.joinable()) _acceptor.join();
      if (_garbage_collector.joinable()) _garbage_collector.join();

      std::lock_guard<std::mutex> lock(_connections_lock);
      _connections.clear();

      _notifier.close();
    }
  }

  size_t multithread::n_connections() const noexcept {
    return _connections.size();
  }

  result<void> multithread::enable(int fd) {
    _fd = fd;
    return notifier::create()
      .and_then(bind_this(&multithread::start_notifier))
      .and_then(bind_this(&multithread::start_acceptor, fd))
      ;
  }

  int multithread::fd() const noexcept {
    return _fd;
  }

  result<void> multithread::start_notifier(notifier &&x) noexcept {
    _notifier = std::move(x);
    return {};
  }

  result<void> multithread::start_acceptor(int fd) noexcept {
    try {
      auto ntfr = notifier::create();
      if (!ntfr) return make_error(error_code::MT_POLICY_FAILED);
      _acceptor_ready = std::move(*ntfr);

      _stop = false;
      _acceptor
        = std::thread
        ( bind_this(&multithread::acceptor, fd)
        );

      auto acceptor_ready = _acceptor_ready.get_fd(notifier::type::RECEIVER);
      auto wait_for_acceptor = detail::pollfds(detail::poll_timeout, {
        {acceptor_ready, {POLLIN, POLLPRI}}
      });

      while (!_stop.load()) {
        auto events = wait_for_acceptor();
        if (events && events->contains(acceptor_ready)) return {};
      }

      return {};
    } catch (const std::exception &e) {
      return make_error(error_code::MT_POLICY_FAILED, e);
    }
  }

  void multithread::collect_garbage() {
    auto exit = _notifier.get_fd(notifier::type::RECEIVER);

    while (!_stop.load()) {
      detail::pollfds::events_for_fd fds {{exit, {POLLIN, POLLPRI}}};

      {
        auto lock = std::lock_guard(_connections_lock);
        for (auto &&x : _connections) {
          if (x.conn) fds[x.conn->fd()] = {POLLHUP, POLLRDHUP};
        }
      }

      auto events = detail::pollfds(detail::poll_timeout, fds)();

      if (!events) continue;
      if (events->contains(exit)) return;

      auto lock = std::lock_guard(_connections_lock);

      _connections.remove_if([&events] (auto &&x) {
        return !x.conn || !x.conn->is_open() || events->contains(x.conn->fd());
      });
    }
  }

  void multithread::acceptor(int fd) {
    auto exit = _notifier.get_fd(notifier::type::RECEIVER);
    auto wait_for_events = detail::pollfds(detail::poll_timeout,
      { {fd, {POLLIN, POLLPRI}}
      , {exit, {POLLIN, POLLPRI}}
    });

    _garbage_collector = std::thread(bind_this(&multithread::collect_garbage));

    _acceptor_ready.notify();
    while (!_stop.load()) {
      auto events = wait_for_events();

      if (!events || events->empty()) continue;
      if (events->contains(exit)) return;

      if (events->contains(fd)) {
        auto conn_fd = ::accept(fd, NULL, NULL);

        auto& rc = _connections.emplace_back(conn_fd);
        rc.thread = std::thread([this, &rc](){
          this->invoke(std::move(rc.conn));
        });
      }
    }
  }

  int multithread::sync() {
    fmap_connections(&connection::sync);
    return 0;
  }

  int multithread::overflow(int c) {
    using namespace std::placeholders;
    fmap_connections(std::bind(&connection::overflow, _1, c));
    return c;
  }

  std::streamsize multithread::xsputn(const char* s, std::streamsize n) {
    using namespace std::placeholders;
    fmap_connections(std::bind(&connection::xsputn, _1, s, n));
    return n;
  }

  int multithread::underflow() {
    return fmap_first_connection(&connection::underflow);
  }

  int multithread::uflow() {
    return fmap_first_connection(&connection::uflow);
  }

  std::streamsize multithread::xsgetn(char* s, std::streamsize n) {
    using namespace std::placeholders;
    return fmap_first_connection(std::bind(&connection::xsgetn, _1, s, n));
  }

  int multithread::pbackfail(int c) {
    using namespace std::placeholders;
    return fmap_first_connection(std::bind(&connection::pbackfail, _1, c));
  }

  std::streamsize multithread::showmanyc() {
    return fmap_first_connection(&connection::showmanyc);
  }

} /* namespace posix::net::policy */
