#include <cpp_stream_socket/policies/single_thread.hpp>

#include <functional>
#include <algorithm>

#include <sys/socket.h>
#include <unistd.h>

#include <cpp_stream_socket/utils/functional.hpp>
#include <cpp_stream_socket/utils/poll.hpp>
#include <cpp_stream_socket/config.hpp>

namespace posix::net::policy {

  single_thread::~single_thread() {
    close();
  }

  void single_thread::close() noexcept {
    if (!_stop.load()) {
      _stop = true;
      ::close(_fd);
      _notifier.notify();
      if (_acceptor.joinable()) _acceptor.join();
      _notifier.close();
    }
  }

  result<void> single_thread::enable(int fd) noexcept {
    _fd = fd;
    return notifier::create()
      .and_then(bind_this(&single_thread::start_notifier))
      .and_then(bind_this(&single_thread::start_acceptor, fd))
      ;
  }

  int single_thread::fd() const noexcept {
    return _fd;
  }

  size_t single_thread::n_connections() const noexcept {
    return 0;
  }

  result<void> single_thread::start_notifier(notifier &&x) noexcept {
    _notifier = std::move(x);
    return {};
  }

  result<void> single_thread::start_acceptor(int fd) noexcept {
    try {
      auto ntfr = notifier::create();
      if (!ntfr) return make_error(error_code::ST_POLICY_FAILED);
      _acceptor_ready = std::move(*ntfr);

      _stop = false;
      _acceptor
        = std::thread
        ( bind_this(&single_thread::acceptor, fd)
        );

      auto acceptor_ready = _acceptor_ready.get_fd(notifier::type::RECEIVER);
      auto wait_for_acceptor = detail::pollfds(detail::poll_timeout, {
        {acceptor_ready, {POLLIN, POLLPRI}}
      });

      while (!_stop.load()) {
        auto events = wait_for_acceptor();
        if (events && events->contains(acceptor_ready)) return {};
      }

      return {};
    } catch (const std::exception &e) {
      return make_error(error_code::ST_POLICY_FAILED, e);
    }
  }

  void single_thread::acceptor(int fd) noexcept {
    auto exit = _notifier.get_fd(notifier::type::RECEIVER);
    auto wait_for_events = detail::pollfds(detail::poll_timeout,
      { {fd, {POLLIN, POLLPRI}}
      , {exit, {POLLIN, POLLPRI}}
    });

    _acceptor_ready.notify();
    while (!_stop.load()) {
      auto events = wait_for_events();

      if (!events || events->empty()) continue;
      if (events->contains(exit)) return;

      if (events->contains(fd)) {
        auto conn_fd = ::accept(fd, NULL, NULL);
        this->invoke(connection::create(conn_fd));
      }
    }
  }

  int single_thread::sync() {
    return EOF;
  }

  int single_thread::overflow(int c) {
    return c;
  }

  std::streamsize single_thread::xsputn(const char*, std::streamsize n) {
    return n;
  }

  int single_thread::underflow() {
    return EOF;
  }

  int single_thread::uflow() {
    return EOF;
  }

  std::streamsize single_thread::xsgetn(char*, std::streamsize n) {
    return n;
  }

  int single_thread::pbackfail(int c) {
    return c;
  }

  std::streamsize single_thread::showmanyc() {
    return EOF;
  }

} /* namespace posix::net::policy */
