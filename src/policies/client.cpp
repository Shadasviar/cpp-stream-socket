#include <cpp_stream_socket/policies/client.hpp>

#include <cpp_stream_socket/utils/functional.hpp>

namespace posix::net::policy {

  client::~client() {
    close();
  }

  result<void> client::enable(int fd) noexcept {
    return connection::create(fd)
      .and_then(bind_this(&client::set_connection))
      ;
  }

  result<void> client::set_connection(connection &&conn) {
    _connection = std::move(conn);
    return {};
  }

  void client::close() noexcept {
    _connection.close();
  }

  int client::fd() const noexcept {
    return _connection.fd();
  }

  size_t client::n_connections() const noexcept {
    return _connection.is_open() ? 1 : 0;
  }

  connection& client::conn() noexcept {
    return _connection;
  }

  int client::sync() {
    return _connection.sync();
  }

  int client::overflow(int c) {
    return _connection.overflow(c);
  }

  std::streamsize client::xsputn(const char* s, std::streamsize n) {
    return _connection.xsputn(s, n);
  }

  int client::underflow() {
    return _connection.underflow();
  }

  int client::uflow() {
    return _connection.uflow();
  }

  std::streamsize client::xsgetn(char* s, std::streamsize n) {
    return _connection.xsgetn(s, n);
  }

  int client::pbackfail(int c) {
    return _connection.pbackfail(c);
  }

  std::streamsize client::showmanyc() {
    return _connection.showmanyc();
  }

} /* namespace posix::net::policy */
