BUILD_DIR=$(shell pwd)/build

all:
	cmake -B${BUILD_DIR} -DBUILD_TESTS=1 .
	cmake --build ${BUILD_DIR}

clean:
	rm -rf ${BUILD_DIR}

test:
	${BUILD_DIR}/tests/cpp_stream_socket_tests --gtest_throw_on_failure

cppcheck:
	cppcheck \
	--enable=warning \
	--enable=performance \
	--enable=portability \
	--error-exitcode=1 \
	src/* \
	-Iinclude
