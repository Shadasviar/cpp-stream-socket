# Find version from git describe command in format v.major.minor-patch-hash
# Your git tags must have format of v.major.minor

set(GIT_REPO_PATH
  "${CMAKE_CURRENT_SOURCE_DIR}/.git"
  CACHE FILEPATH
  "Path of the .git file of the repository"
)

execute_process(
  COMMAND git --git-dir "${GIT_REPO_PATH}" describe
  OUTPUT_VARIABLE GIT_OUTPUT
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

string(
  REGEX
  MATCH "([0-9]+)\.?([0-9]+)?[\.\-]?([0-9]+)?[\.\-]?(.*)"
  GIT_MATCHED
  "${GIT_OUTPUT}"
)

function(get_match MATCH_NAME OUT_NAME DEFAULT_VALUE)
  if (DEFINED ${MATCH_NAME})
    SET(${OUT_NAME} ${${MATCH_NAME}} PARENT_SCOPE)
  else()
    SET(${OUT_NAME} ${DEFAULT_VALUE} PARENT_SCOPE)
  endif()
ENDFUNCTION()

get_match(CMAKE_MATCH_1 GIT_VERSION_MAJOR "0")
get_match(CMAKE_MATCH_2 GIT_VERSION_MINOR "0")
get_match(CMAKE_MATCH_3 GIT_VERSION_PATCH "0")
get_match(CMAKE_MATCH_4 GIT_VERSION_HASH "dirty")

set(GIT_VERSION_FULL
  "${GIT_VERSION_MAJOR}.${GIT_VERSION_MINOR}.${GIT_VERSION_PATCH}"
)
